/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;


import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fer
 */
@WebServlet(name = "ServletFernando", urlPatterns = {"/ServletFernando"})
public class ServletFernando extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //////////////////////////////////////////////////////////
        out.println("<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "    <meta charset='utf-8'>\n"
                + "    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n"
                + "    <title>Listado de productos</title>\n"
                + "    <meta name='viewport' content='width=device-width, initial-scale=1'>\n"
                + "    <link rel='stylesheet' type='text/css' media='screen' href='css/main.css'>\n"
                + "    <script src='main.js'></script>\n"
                + "</head>\n"
                + "<body>\n"
                + "    <header>Encabezado</header>\n"
                + "    <main>");

//        out.println("<p>nombre</p>");
//
//        out.println("<h1>Servlet NewServlet at " + request.getContextPath() + "</h1>");

        String consultaSql = "SELECT * FROM productos;";
        Connection miConexion = null;
        
        String nombre;
        String imagen;
        String descripcion;
        int precio;
        int stock;
       
        
        try {
            miConexion = DB.getInstance().getConnection();
            PreparedStatement miPreparativo = miConexion.prepareStatement(consultaSql);
            ResultSet miResultado = miPreparativo.executeQuery();
            System.out.println(miResultado.getString("nombre"));
            int contador = 0;
            Producto miProducto = new Producto();
            while (miResultado.next()) {
                System.out.println("dfgdfgdf");
                out.println("<p>" + contador + "</p>");
                contador++;
                
                miProducto.foto = miResultado.getString("foto") + "dfkldsfosjdf";
                miProducto.nombre = miResultado.getString("nombre");
                miProducto.precio = miResultado.getString("precio");
                miProducto.cantidad = miResultado.getString("cantidad");
                miProducto.descripcion = miResultado.getString("descripcion");
                
                System.out.println(miProducto);
                
                out.println("<article>");
                out.println("<img src=' " + miProducto.foto + "' width='200' />");
                out.println("<div>Nombre: " + miProducto.nombre + "</div>");
                out.println("<div>Precio: " + miProducto.precio + " </div>");
                out.println("<div>Cantidad: " + miProducto.cantidad + " </div>");
                out.println("<div>Descripción: " + miProducto.descripcion + " </div>");
                
                out.println("</article>");
                
                out.println("dfsdfdsfsdfsdfsdfsdf");
            }

        } catch (ClassNotFoundException miExepcion) {
            out.println("Clase no encontrada" + miExepcion);
        } catch (SQLException malSQL) {
            out.println("Error SQL1: " + malSQL);
        }finally {
            try {
                if (miConexion != null) {
                    miConexion.close();
                }

            } catch (SQLException quePaso) {
                System.out.println("Error SQL2: " + quePaso);
            }
        }

        //////////////////////////////////////////////////////////////
        out.println("</main>\n"
                + "    <footer></footer>\n"
                + "</body>\n"
                + "</html>");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}